import React from 'react';
import {
    StyleSheet,
    View,
    AsyncStorage,
    TextInput,
    ScrollView
} from 'react-native';
import { 
    Header, 
    ListItem, 
    CheckBox, 
    Text, 
    Icon 
} from 'react-native-elements';

const icon = {
    done: 'check',
    undone: 'check-box-outline-blank'
}

const viewPadding = 10;

export default class Main extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            arrTodo: [],
            text: '',
            idTodoEditing: null,
            textEditing: ''
        }
    };

    changeTextHandler = text => {
        this.setState({ text: text });
    };

    /// Enable mode edit todo
    /// if have a todo edit before but be not updated
    ///         update that todo
    /// set idTodoEditing, textEditing
    enableEditTodo = (keyID) =>{
        // console.log(keyID);
        this.setState(prevState => {
            let {arrTodo, text, textEditing, idTodoEditing} = prevState;
            // save todo editing before
            if(arrTodo[idTodoEditing])
                arrTodo[idTodoEditing].title = textEditing;

            idTodoEditing = keyID;
            textEditing = arrTodo[keyID].title;
            // console.log(textEditing);

            return {
                'arrTodo': arrTodo,
                'text': text,
                'idTodoEditing': idTodoEditing,
                'textEditing': textEditing
            };
        })
    };

    /// Edit todo
    /// update titile todo using idTodoEditing and textEditing
    editTodo = () =>{
        this.setState(prevState => {
                let {arrTodo, text, textEditing, idTodoEditing} = prevState;
                if(arrTodo[idTodoEditing]){
                    arrTodo[idTodoEditing].title = textEditing;
                }
                
                return {
                    'arrTodo': arrTodo,
                    'text': text,
                    'idTodoEditing': null,
                    'textEditing': null
                };
            }, 
            //callback after add todo to array callback 'll save array todo to local storage
            () => { Todos.save(this.state.arrTodo); }
        );
    }

    /// change state done todo using keyID in array
    doneTodo = (keyID) => {
        this.setState(prevState => {
                let {arrTodo, text, textEditing, idTodoEditing} = prevState;
               
                arrTodo[keyID].done = !arrTodo[keyID].done;
                return {
                    'arrTodo': arrTodo,
                    'text': text,
                    'idTodoEditing': idTodoEditing,
                    'textEditing': textEditing
                };
            }, 
            //callback after add todo to array callback 'll save array todo to local storage
            () => { Todos.save(this.state.arrTodo); }
        );
    }

    /// add new todo
    /// Structure todo : { 'title': 'Todo name', 'done': false}
    addTodo = () => {
        let notEmpty = this.state.text.trim().length > 0;
    
        if (notEmpty) {
            this.setState(prevState => {
                    let { arrTodo, text } = prevState;
                    return {
                        'arrTodo': arrTodo.concat({title: text, done: false}),
                        'text': "",
                        'idTodoEditing': null,
                        'textEditing': null
                    };
                }, 
                //callback after add todo to array callback 'll save array todo to local storage
                () => { Todos.save(this.state.arrTodo); }
            );
        }
    };


    // delete todo using keyID in array
    deleteTodo = (keyID) => {

        // run ok
        this.state.arrTodo.splice(keyID, 1); 

        this.setState({
                arrTodo: this.state.arrTodo,
                'text': '',
                'idTodoEditing': null,
                'textEditing': null
            }, 
            //callback after add todo to array callback 'll save array todo to local storage
            () => { Todos.save(this.state.arrTodo); }
        );

        // // run fault
        // // bug: nếu đang edit một todo mà chưa lưu, lại xoá đúng todo đó, thì các todo khác bị xoá chỉ còn lại duy nhất todo muốn xoá
        // this.setState(prevState => {
        //         let { arrTodo, text } = prevState;
        //         return {
        //             'arrTodo': arrTodo.splice(keyID, 1),
        //             'text': "",
        //             'idTodoEditing': null,
        //             'textEditing': null
        //         };
        //     }, 
        //     //callback after add todo to array callback 'll save array todo to local storage
        //     () => { Todos.save(this.state.arrTodo); }
        // );
    };


    componentDidMount() {    
        console.log("Did mount")
        Todos.getAll(todos => this.setState({ arrTodo: todos || [] }));
    };
    
    render() {
        return(
            <View >
                <Header
                    centerComponent={{ text: 'Todo List', style: { color: '#fff' } }}
                />
                <ScrollView style={styles.scrollContainer}>
                    <View >
                    {
                        this.state.arrTodo.map((todo, i) => (
                            <ListItem
                                key={i}
                                // onPress={()=>{this.enableEditTodo(i)}}
                                onPress={()=> this.doneTodo(i)}
                                onLongPress={()=>{this.enableEditTodo(i)}}
                                
                                title={todo.title}
                                titleStyle={todo.done? {textDecorationLine: 'line-through', fontWeight: '600', width: '100%'} : {fontWeight: '600', width: '100%'}}

                                leftIcon={{ 
                                    name: todo.done? icon.done : icon.undone , 
                                    color: todo.done? "green" : ''}
                                }
                                leftIconOnPress={()=> this.doneTodo(i)}

                                rightIcon={{name:'clear', color:'red'}}
                                onPressRightIcon={()=>{this.deleteTodo(i)}}

                                textInput={(this.state.idTodoEditing == i) ? true : false}
                                textInputEditable={(this.state.idTodoEditing == i) ? true : false}
                                textInputValue={this.state.textEditing}
                                textInputAutoFocus={(this.state.idTodoEditing == i) ? true : false}
                                textInputOnChangeText={(text)=>{
                                    this.setState({ 'textEditing': text });
                                }}
                                textInputStyle={{
                                    textAlign: 'left', 
                                    position: 'absolute', 
                                    backgroundColor: 'white',
                                    // right: 1000
                                    left: '-100%',
                                    width: '300%'
                                }}
                                textInputOnBlur={()=>{
                                    this.editTodo()
                                }}
                            />
                        ))
                    }
                    </View>

                    <TextInput
                        style={styles.textInput}
                        onChangeText={this.changeTextHandler}
                        onSubmitEditing={this.addTodo}
                        value={this.state.stateEdit? '' : this.state.text}
                        placeholder="Add Tasks"
                        returnKeyType="done"
                        returnKeyLabel="done"
                    />
                </ScrollView>
            </View>
        );
    }

}

let Todos = {
    getAll(callback){
        return AsyncStorage.getItem("Todos", (err, todos)=>{
            console.log(JSON.parse(todos));
            console.log("=====GET=========")
            callback(JSON.parse(todos));
        });
    },
    save(todos){
        console.log(todos);
        AsyncStorage.setItem("Todos", JSON.stringify(todos));
        console.log("======Save success=======");
    }
}

const styles = StyleSheet.create({

    scrollContainer: {
        width: "100%",
        marginBottom: 100
    },
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF",
        padding: viewPadding,
        paddingTop: 20
    },
    textInput: {
        height: 40,
        paddingRight: 10,
        paddingLeft: 10,
        borderColor: "gray",
        borderWidth: 0,
        width: "100%"
    }
});